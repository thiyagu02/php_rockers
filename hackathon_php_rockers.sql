-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 01, 2019 at 12:02 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.2.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hackathon_php_rockers`
--

-- --------------------------------------------------------

--
-- Table structure for table `beneficiary`
--

CREATE TABLE `beneficiary` (
  `id` int(2) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `beneficiary_id` bigint(20) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `modified_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `beneficiary`
--

INSERT INTO `beneficiary` (`id`, `user_id`, `beneficiary_id`, `status`, `created_date`, `modified_date`) VALUES
(1, 8, 1, 1, '2019-11-01 08:43:28', '2019-11-01 08:43:28'),
(2, 8, 2, 1, '2019-11-01 08:43:28', '2019-11-01 08:43:28'),
(3, 1, 2, 1, '2019-11-01 08:44:23', '2019-11-01 08:44:23'),
(4, 1, 4, 1, '2019-11-01 08:44:23', '2019-11-01 08:44:23');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role_master`
--

CREATE TABLE `role_master` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(200) NOT NULL,
  `from_account_user_id` int(200) NOT NULL,
  `to_account_user_id` int(200) NOT NULL,
  `transaction_date` datetime NOT NULL,
  `transaction_status` int(200) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `created_on` datetime NOT NULL,
  `transaction_id` varchar(200) NOT NULL,
  `type` varchar(200) NOT NULL,
  `comments` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `from_account_user_id`, `to_account_user_id`, `transaction_date`, `transaction_status`, `amount`, `created_on`, `transaction_id`, `type`, `comments`) VALUES
(1, 1, 2, '2019-10-01 09:22:17', 1, '200.00', '2019-11-01 09:22:17', 'EID2222', '1', 'Test Transaction'),
(2, 7, 4, '2019-10-01 09:22:17', 1, '750.00', '2019-11-01 09:22:17', 'EID2223', '1', 'Last month Debt'),
(3, 6, 11, '2019-09-02 09:22:17', 1, '600.00', '2019-11-01 09:22:17', 'EID2224', '1', 'Pay back'),
(4, 4, 3, '2019-09-03 09:22:17', 1, '3400.00', '2019-11-01 09:22:17', 'EID2225', '1', 'For Landline Bill'),
(5, 2, 7, '2019-09-04 09:22:17', 1, '400.00', '2019-11-01 09:22:17', 'EID2226', '1', 'Monthly bills'),
(6, 8, 9, '2019-09-05 09:22:17', 1, '300.00', '2019-11-01 09:22:17', 'EID2227', '1', 'Test Amount'),
(7, 8, 9, '2019-09-05 09:22:17', 1, '2300.00', '2019-11-01 09:22:17', 'EID2228', '1', ' Amount'),
(8, 3, 2, '2019-09-06 09:22:17', 1, '20.00', '2019-11-01 09:22:17', 'EID2229', '1', 'Testing Transaction'),
(9, 7, 6, '2019-09-07 09:22:17', 1, '3320.00', '2019-11-01 09:22:17', 'EID2230', '1', 'Tax Bills'),
(10, 9, 3, '2019-09-08 09:22:17', 1, '50.00', '2019-11-01 09:22:17', 'EID2231', '1', 'Monthly debt');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `role_id`, `username`, `password`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'SOBHANBABU', 'MOPURU', '2', 'CUST_1001', '$2y$10$OkXN.bkSwjatutlqTYfVCOk1RqO6VMFwfc3Y1JtpMYwUTSUI3n242', 1, NULL, '2019-11-01 07:34:09', '2019-11-01 07:34:09'),
(2, 'Vishwa', 'R', '2', 'CUST_1002', '$2y$10$X6247mtU.qBgmGKNyctnK.F2II6Yrd2dSaDsSjtbDHz6BR7nrpI4y', 1, NULL, '2019-11-01 07:52:01', '2019-11-01 07:52:01'),
(3, 'Thilak', 'Kumar', '2', 'CUST_1003', '$2y$10$X6247mtU.qBgmGKNyctnK.F2II6Yrd2dSaDsSjtbDHz6BR7nrpI4y', 1, NULL, '2019-11-01 07:52:01', '2019-11-01 07:52:01'),
(4, 'Kiran', 'K', '2', 'CUST_1004', '$2y$10$X6247mtU.qBgmGKNyctnK.F2II6Yrd2dSaDsSjtbDHz6BR7nrpI4y', 1, NULL, '2019-11-01 07:52:01', '2019-11-01 07:52:01'),
(5, 'Uttara', 'Iyer', '2', 'CUST_1005', '$2y$10$X6247mtU.qBgmGKNyctnK.F2II6Yrd2dSaDsSjtbDHz6BR7nrpI4y', 1, NULL, '2019-11-01 07:52:01', '2019-11-01 07:52:01'),
(6, 'Louis', 'R', '2', 'CUST_1006', '$2y$10$X6247mtU.qBgmGKNyctnK.F2II6Yrd2dSaDsSjtbDHz6BR7nrpI4y', 1, NULL, '2019-11-01 07:52:01', '2019-11-01 07:52:01'),
(7, 'Ram', 'Kumar', '2', 'CUST_1007', '$2y$10$X6247mtU.qBgmGKNyctnK.F2II6Yrd2dSaDsSjtbDHz6BR7nrpI4y', 1, NULL, '2019-11-01 07:52:01', '2019-11-01 07:52:01'),
(8, 'Lalith', 'Raj', '2', 'CUST_1008', '$2y$10$X6247mtU.qBgmGKNyctnK.F2II6Yrd2dSaDsSjtbDHz6BR7nrpI4y', 1, NULL, '2019-11-01 07:52:01', '2019-11-01 07:52:01'),
(9, 'Naveen', 'Raj', '2', 'CUST_1009', '$2y$10$X6247mtU.qBgmGKNyctnK.F2II6Yrd2dSaDsSjtbDHz6BR7nrpI4y', 1, NULL, '2019-11-01 07:52:01', '2019-11-01 07:52:01'),
(10, 'Hari', 'Shankar', '2', 'CUST_1010', '$2y$10$X6247mtU.qBgmGKNyctnK.F2II6Yrd2dSaDsSjtbDHz6BR7nrpI4y', 1, NULL, '2019-11-01 07:52:01', '2019-11-01 07:52:01'),
(11, 'Deepthi', 'Venkat', '2', 'CUST_1011', '$2y$10$X6247mtU.qBgmGKNyctnK.F2II6Yrd2dSaDsSjtbDHz6BR7nrpI4y', 1, NULL, '2019-11-01 07:52:01', '2019-11-01 07:52:01'),
(12, 'SOBHANBABU', 'MOPURU', '1', 'test123', '$2y$10$alehX0Gzo/b9F1HDGJ0n7.6CMY1Xei9LQb22mE1FyMQxewFRGLeU6', 1, NULL, '2019-11-01 09:26:30', '2019-11-01 09:26:30'),
(13, 'TEST', 'TEST', '1', 'CUST_00', '$2y$10$3QjD.ECWQpKDCB1e1hEeCuX6kD5W/0pzTKoQ6F..9pO9bAcMjY78i', 1, NULL, '2019-11-01 09:37:05', '2019-11-01 09:37:05');

-- --------------------------------------------------------

--
-- Table structure for table `user_account_details`
--

CREATE TABLE `user_account_details` (
  `user_account_id` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `account_type` int(11) NOT NULL,
  `account_no` varchar(255) NOT NULL,
  `available_balance` decimal(10,2) NOT NULL,
  `created_date` datetime NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_account_details`
--

INSERT INTO `user_account_details` (`user_account_id`, `user_id`, `account_type`, `account_no`, `available_balance`, `created_date`, `status`) VALUES
(1, 1, 1, '500001', '800.00', '2019-11-01 03:06:27', 1),
(2, 2, 1, '500002', '2000.00', '2019-11-01 03:06:27', 1),
(3, 3, 1, '500003', '2000.00', '2019-11-01 03:06:27', 1),
(4, 4, 1, '500004', '3400.00', '2019-11-01 03:06:27', 1),
(5, 5, 1, '500005', '3400.00', '2019-11-01 03:06:27', 1),
(6, 6, 1, '500006', '52500.00', '2019-11-01 03:06:27', 1),
(7, 7, 1, '500007', '77500.00', '2019-11-01 03:06:27', 1),
(8, 8, 1, '500008', '500.00', '2019-11-01 03:06:27', 1),
(9, 9, 1, '500009', '1100.00', '2019-11-01 03:06:27', 1),
(10, 10, 1, '500010', '5200.00', '2019-11-01 03:06:27', 1),
(11, 11, 1, '500011', '85300.00', '2019-11-01 03:06:27', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `beneficiary`
--
ALTER TABLE `beneficiary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_master`
--
ALTER TABLE `role_master`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_account_details`
--
ALTER TABLE `user_account_details`
  ADD PRIMARY KEY (`user_account_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `beneficiary`
--
ALTER TABLE `beneficiary`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `role_master`
--
ALTER TABLE `role_master`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `user_account_details`
--
ALTER TABLE `user_account_details`
  MODIFY `user_account_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
