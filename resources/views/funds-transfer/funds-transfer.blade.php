@extends("layouts.app")
@section("content")
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-3">
            <ul class="list-group">
            <li class="list-group-item"><a href="account-summary">Account Summary</a></li>
            <li class="list-group-item"><a href="account-details">Account Details</a></li>
            <li class="list-group-item"><a href="funds-transfer">Funds Transfer</a></li>
          </ul>
        </div>
        <div class="col-md-9">
			<h2>Funds Transfer</h2>
			<form>
			  <div class="form-group">
				<label for="exampleInputEmail1">From Account</label>
				<input type="text" disabled="disabled" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="500001">
				<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
			  </div>
			  <div class="form-group">
				<label for="exampleInputPassword1">To Account</label>
				<select name="to_account" id="to_account">
					<option value="">500002</option>
					<option value="">500003</option>
					<option value="">500004</option>
					<option value="">500005</option>
				</select>
			  </div>
			   <div class="form-group">
				<label for="amount">Amount</label>
			   <input type="text"  class="form-control" id="amount" aria-describedby="amount" placeholder="Amount">
			  </div> 
			   <div class="form-group">
				<label for="amount">Comments</label>
			   <textarea class="input" name="comments" rows="4" cols="50">
			   </textarea>
			  </div> 
			  <button type="submit" class="btn btn-primary">Submit</button>
			</form>
		</div>
	</div>
@endsection