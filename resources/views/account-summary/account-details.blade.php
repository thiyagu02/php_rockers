@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-3">
            <ul class="list-group">
            <li class="list-group-item"><a href="account-summary">Account Summary</a></li>
            <li class="list-group-item"><a href="account-details">Account Details</a></li>
            <li class="list-group-item"><a href="funds-transfer">Funds Transfer</a></li>
          </ul>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">Account Details</div>
                <div class="card-body">
                    <table class="table">
						<thead>
						  <tr>
							<th>Account Number</th>						
							<th>Available Balance</th>
							<th>Creation Date</th>
						  </tr>
						</thead>
						<tbody>
						  <tr>
							
							<td>{{$data->account_no}}</td>
							<td>{{$data->available_balance}}</td>
							<td>{{$data->created_date}}</td>
						  </tr>
						 
						</tbody>
					  </table>
                </div>
            </div>
        </div>
    </div>
	<div class="row justify-content-center">
	<div class="col-md-3"></div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">Transaction Summary</div>
                <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>From</th>
                <th>To</th>
                <th>Debit</th>
                <th>Crdit</th>
                <th>Amount</th>
                <th>Date</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="6">No data found.</td>
            </tr>
			</tbody>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
