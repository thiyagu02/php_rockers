@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-3">
            <ul class="list-group">
            <li class="list-group-item"><a href="account-summary">Account Summary</a></li>
            <li class="list-group-item"><a href="account-details">Account Details</a></li>
            <li class="list-group-item"><a href="funds-transfer">Funds Transfer</a></li>
          </ul>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">Account Summary</div>
                <div class="card-body">
                    <table class="table">
						<thead>
						  <tr>
							<th>Account Holder Name</th>
							<th>Account Number</th>
							<th>Account Type</th>							
							<th>Available Balance</th>
							<th>Account Details</th>
						  </tr>
						</thead>
						<tbody>
						  <tr>
							<td>{{$data->first_name.' '.$data->last_name}}</td>
							<td>{{$data->account_no}}</td>
							<td>{{$data->account_type==1?'Savings':($data->account_type==2?'Current':'DEMAT')}}</td>
							<td>{{$data->available_balance}}</td>
							<td><a href="account-details">View</a></td>
						  </tr>
						 
						</tbody>
					  </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
