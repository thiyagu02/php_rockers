<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;

class AccountSummary extends Model
{
    public static function getAccSummary(){
		$user=DB::table('users')->leftJoin('user_account_details', 'users.id','=','user_account_details.user_id')->where('users.id',1)->first();
		return $user;
	}
}
