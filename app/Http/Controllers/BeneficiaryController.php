<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Beneficiary;
use View;

class BeneficiaryController extends Controller
{
    public function getBeneficiary(){
	 $res = Beneficiary::getBeneficiary();
	 $data['data'] = $res;
	 $view = View::make('getBeneficiary.agetBeneficiary', $data);

    return $view->render();
	}
}
