<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use \App\FundsTransfer as FundsTransfer;
use \App\Beneficiary as Beneficiary;
class FundsTransferController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	
	
	 /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    public function index()
    {
		// $logged_in_user = auth()->user()->id;
		// $get_logged = FundsTransfer::all();
		return view('funds-transfer/funds-transfer',compact(''));
    }
}
