<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AccountSummary;
use View;

class AccountSummaryController extends Controller
{
    public function getAccSummary(){
	 $res = AccountSummary::getAccSummary();
	 $data['data'] = $res;
	 $view = View::make('account-summary.account-summary',$data);

    return $view->render();
	}
	public function getAccDetails(){
	 $res = AccountSummary::getAccSummary();
	 $data['data'] = $res;
	 $view = View::make('account-summary.account-details',$data);

    return $view->render();
	}
}
